package questions

import java.io.{File, PrintWriter}

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**

  40%+30%
  1. 試著從dataset/small-ratings.csv 計算每部電影ID的平均評價
  2. 試著利用movieId 從dataset/movie.csv join出title
  3. 試著印出評價最高的前10名電影，格式符合(movieId, average_rating, title )
  4. (加分題30%)試著從dataset/small-ratings.csv 計算每部電影ID的評價的平均數及標準差

  */
object Top10App extends App{
  val conf = new SparkConf().setAppName("top10-ratings")
    .setMaster("local[*]")
  val sc = new SparkContext(conf)

/**
  1. 試著從dataset/small-ratings.csv 計算每部電影ID的平均評價(20%)
  small-ratings.csv 格式如下
  userId, movieId, rating,  timestamp
  107799, 30707,   4.5,     1128005714
  28398,  3701,    1.5,     1075601839
  131283, 1281,    4.0,     947176194
  76688,  4427,    4.5,     1238740344

  提示:
  僅取出movieId及rating兩個欄位即可
  */



  val ratings:RDD[(Int,Double)]={
    sc.textFile("dataset/small-ratings.csv")
      .map(str=>str.split(","))
      .filter(strs=>strs(1)!="movieId")
      .map(strs=>strs(1).toInt->strs(2).toDouble)
    ratings.take(10).foreach(println)

  }


  /**
   RDD[(movieId,(average,count))]
   建議實現averageAndCountRatingByMovieId，可幫助你完成4. 加分題！！
   沒實作也沒關係，不影響分數
    */
  val averageAndCountRatingByMovieId: RDD[(Int, (Double, Int))] =
    ratings.reduceByKey((acc,curr)=>acc+curr)
  averageAndCountRatingByMovieId.take(10).foreach(println)

  // RDD[(movieId,averageRating)]
  val averageRatingByMovieId:RDD[(Int,Double)]= ratings.mapValues(v=>v->1)
      .reduceByKey({(acc,curr)=>(acc._1+curr._1)->(acc._2+curr._2)})
      .mapValues(kv=>(kv._1)/(kv._2))
  averageRatingByMovieId.take(10).foreach(println)

  /**
  2. 試著利用movieId 從dataset/movies.csv join出title(10%)
  movies.csv 格式如下
  movieId,  title,                    genres
  1,        Toy Story (1995),         Adventure|Animation|Children|Comedy|Fantasy
  2,        Jumanji (1995),           Adventure|Children|Fantasy
  3,        Grumpier Old Men (1995),  Comedy|Romance
  4,        Waiting to Exhale (1995), Comedy|Drama|Romance


  提示:
  僅取出movieId及title兩個欄位即可
    */

  val movies: RDD[(Int, String)] ={
    sc.textFile("dataset/movies.csv")
      .map(str=>str.split(","))
      .filter(strs=>strs(0)!="movieId")
      .map(strs=>strs(0).toInt->strs(1).toString)
    ratings.take(10).foreach(println)

  }
  val joined: RDD[(Int, (Double, String))]= ???

  /**
  3. 試著印出評價最高的前10名電影，格式符合(movieId, average_rating, title ) (10%)
  提示:
  排序後再取出前10名
    */
  val top10: Array[(Int, (Double, String))] =
    averageRatingByMovieId.sortBy(._.2,false)
     top10.foreach(println)

  /**
  4. (加分題30%)試著從dataset/small-ratings.csv 計算每部電影ID的評價的敘述統計量(stats)
  small-ratings.csv 格式如下
  userId, movieId, rating,  timestamp
  107799, 30707,   4.5,     1128005714
  28398,  3701,    1.5,     1075601839
  131283, 1281,    4.0,     947176194
  76688,  4427,    4.5,     1238740344

  提示:
  僅取出movieId及rating兩個欄位即可
    */

  //Stats(平均數,母體變異數)
  //變異數公式 http://estat.ncku.edu.tw/topic/desc_stat/base/variance.html
  case class Stats(mean:Double,variance:Double)

  // RDD[(movieId,stats )]
  val statsRatingByMovieId:RDD[(Int,Stats)]= ratings.mapValues(v=>v->1)
    .reduceByKey({(acc,curr)=>(acc._1+curr._1)->(acc._2+curr._2)})
    .mapValues()
}
